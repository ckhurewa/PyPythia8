#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

BOOST_PYTHON_MODULE(Particle){

  class_<Particle>("Particle")

    // Read-write descriptor
    .add_property("id"        , (int   (Particle::*)()const) &Particle::id       , (void(Particle::*)(int))     &Particle::id         )
    .add_property("status"    , (int   (Particle::*)()const) &Particle::status   , (void(Particle::*)(int))     &Particle::status     )
    .add_property("mother1"   , (int   (Particle::*)()const) &Particle::mother1  , (void(Particle::*)(int))     &Particle::mother1    )
    .add_property("mother2"   , (int   (Particle::*)()const) &Particle::mother2  , (void(Particle::*)(int))     &Particle::mother2    )
    .add_property("daughter1" , (int   (Particle::*)()const) &Particle::daughter1, (void(Particle::*)(int))     &Particle::daughter1  )
    .add_property("daughter2" , (int   (Particle::*)()const) &Particle::daughter2, (void(Particle::*)(int))     &Particle::daughter2  )
    .add_property("col"       , (int   (Particle::*)()const) &Particle::col      , (void(Particle::*)(int))     &Particle::col        )
    .add_property("acol"      , (int   (Particle::*)()const) &Particle::acol     , (void(Particle::*)(int))     &Particle::acol       )
    .add_property("p"         , (Vec4  (Particle::*)()const) &Particle::p        , (void(Particle::*)(Vec4))    &Particle::p          )
    .add_property("px"        , (double(Particle::*)()const) &Particle::px       , (void(Particle::*)(double))  &Particle::px         )
    .add_property("py"        , (double(Particle::*)()const) &Particle::py       , (void(Particle::*)(double))  &Particle::py         )
    .add_property("pz"        , (double(Particle::*)()const) &Particle::pz       , (void(Particle::*)(double))  &Particle::pz         )
    .add_property("e"         , (double(Particle::*)()const) &Particle::e        , (void(Particle::*)(double))  &Particle::e          )
    .add_property("m"         , (double(Particle::*)()const) &Particle::m        , (void(Particle::*)(double))  &Particle::m          )
    .add_property("scale"     , (double(Particle::*)()const) &Particle::scale    , (void(Particle::*)(double))  &Particle::scale      )
    .add_property("pol"       , (double(Particle::*)()const) &Particle::pol      , (void(Particle::*)(double))  &Particle::pol        )
    .add_property("vProd"     , (Vec4  (Particle::*)()const) &Particle::vProd    , (void(Particle::*)(Vec4))    &Particle::vProd      )
    .add_property("xProd"     , (double(Particle::*)()const) &Particle::xProd    , (void(Particle::*)(double))  &Particle::xProd      )
    .add_property("yProd"     , (double(Particle::*)()const) &Particle::yProd    , (void(Particle::*)(double))  &Particle::yProd      )
    .add_property("zProd"     , (double(Particle::*)()const) &Particle::zProd    , (void(Particle::*)(double))  &Particle::zProd      )
    .add_property("tProd"     , (double(Particle::*)()const) &Particle::tProd    , (void(Particle::*)(double))  &Particle::tProd      )
    .add_property("tau"       , (double(Particle::*)()const) &Particle::tau      , (void(Particle::*)(double))  &Particle::tau        )

    // Read-only descriptor
    .add_property("idAbs"                 , &Particle::idAbs)
    .add_property("statusAbs"             , &Particle::statusAbs)
    .add_property("isFinal"               , &Particle::isFinal)
    .add_property("isRescatteredIncoming" , &Particle::isRescatteredIncoming)
    .add_property("isVisible"             , &Particle::isVisible)
    .add_property("isCharged"             , &Particle::isCharged)
    .add_property("m2"                    , &Particle::m2)
    .add_property("mCalc"                 , &Particle::mCalc)
    .add_property("mCalc"                 , &Particle::mCalc)
    .add_property("m2Calc"                , &Particle::m2Calc)
    .add_property("m2Calc"                , &Particle::m2Calc)
    .add_property("eCalc"                 , &Particle::eCalc)
    .add_property("pT"                    , &Particle::pT)
    .add_property("pT2"                   , &Particle::pT2)
    .add_property("mT"                    , &Particle::mT)
    .add_property("mT2"                   , &Particle::mT2)
    .add_property("pAbs"                  , &Particle::pAbs)
    .add_property("pAbs2"                 , &Particle::pAbs2)
    .add_property("eT"                    , &Particle::eT)
    .add_property("eT2"                   , &Particle::eT2)
    .add_property("theta"                 , &Particle::theta)
    .add_property("phi"                   , &Particle::phi)
    .add_property("thetaXZ"               , &Particle::thetaXZ)
    .add_property("pPos"                  , &Particle::pPos)
    .add_property("pPos"                  , &Particle::pPos)
    .add_property("pNeg"                  , &Particle::pNeg)
    .add_property("y"                     , &Particle::y)  
    .add_property("eta"                   , &Particle::eta)
    .add_property("vDec"                  , &Particle::vDec)
    .add_property("xDec"                  , &Particle::xDec)
    .add_property("yDec"                  , &Particle::yDec)
    .add_property("zDec"                  , &Particle::zDec)
    .add_property("tDec"                  , &Particle::tDec)

    // Event-dependent methods
    .add_property("index"         , &Particle::index)  
    .add_property("statusHepMC"   , &Particle::statusHepMC)  
    .add_property("iTopCopy"      , &Particle::iTopCopy)   
    .add_property("iBotCopy"      , &Particle::iBotCopy)   
    .add_property("iTopCopyId"    , &Particle::iTopCopyId) 
    .add_property("iBotCopyId"    , &Particle::iBotCopyId) 
    .add_property("motherList"    , &Particle::motherList)
    .add_property("daughterList"  , &Particle::daughterList)
    .add_property("sisterList"    , &Particle::sisterList)

    // Non-mutating method
    .def("isAncestor", &Particle::isAncestor)

    // Mutating method
    .def("statusPos"  , &Particle::statusPos  )
    .def("statusNeg"  , &Particle::statusNeg  )
    .def("statusCode" , &Particle::statusCode )
    .def("undoDecay"  , &Particle::undoDecay  )

    // Further output, based on a pointer to a ParticleDataEntry object.
    .add_property( "name"               , &Particle::name)
    .add_property( "m0"                 , &Particle::m0)
    .add_property( "spinType"           , &Particle::spinType)
    .add_property( "chargeType"         , &Particle::chargeType)
    .add_property( "charge"             , &Particle::charge)
    .add_property( "isCharged"          , &Particle::isCharged)
    .add_property( "isNeutral"          , &Particle::isNeutral)
    .add_property( "colType"            , &Particle::colType)
    .add_property( "m0"                 , &Particle::m0)
    .add_property( "mWidth"             , &Particle::mWidth)
    .add_property( "mMin"               , &Particle::mMin)
    .add_property( "mMax"               , &Particle::mMax)
    .add_property( "mSel"               , &Particle::mSel)
    .add_property( "constituentMass"    , &Particle::constituentMass)
    .add_property( "tau0"               , &Particle::tau0)
    .add_property( "mayDecay"           , &Particle::mayDecay)
    .add_property( "canDecay"           , &Particle::canDecay)
    .add_property( "doExternalDecay"    , &Particle::doExternalDecay)
    .add_property( "isResonance"        , &Particle::isResonance)
    .add_property( "isVisible"          , &Particle::isVisible)
    .add_property( "isLepton"           , &Particle::isLepton)
    .add_property( "isQuark"            , &Particle::isQuark)
    .add_property( "isGluon"            , &Particle::isGluon)
    .add_property( "isDiquark"          , &Particle::isDiquark)
    .add_property( "isParton"           , &Particle::isParton)
    .add_property( "isHadron"           , &Particle::isHadron)
    // .add_property( "particleDataEntry"  , &Particle::particleDataEntry) // TODO
  ;


}
