#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;


BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ParticleData_list1, ParticleData::list, 0, 1);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ParticleData_list2, ParticleData::list, 1, 1);

//-------------------------------------------------------------------------------

BOOST_PYTHON_MODULE(ParticleData){

  class_<ParticleData>("ParticleData")
    // Methods
    .def("m0"   , (double (ParticleData::*)(int)) &ParticleData::m0)
    .def("list" , (void   (ParticleData::*)(bool, bool, ostream&)) &ParticleData::list, ParticleData_list1())
    .def("list" , (void   (ParticleData::*)(int, ostream&))        &ParticleData::list, ParticleData_list2())
  ; 

}

 

