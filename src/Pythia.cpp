#include "Pythia8/Pythia.h"
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

using namespace Pythia8;
using namespace boost::python;

// Careful, need to pass by reference
const bool pythia_setUserHooks(Pythia& pythia, UserHooks& hook){
  return pythia.setUserHooksPtr(&hook);
}

// Macro has to also be outside
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(pythia_readString      , Pythia::readString      , 1, 2);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(pythia_readFile        , Pythia::readFile        , 1, 3);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(pythia_forceTimeShower , Pythia::forceTimeShower , 3, 4);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(pythia_forceHadronLevel, Pythia::forceHadronLevel, 0, 1);

//------------------------------------------------------------------------------

BOOST_PYTHON_MODULE(Pythia){

  // Used internally
  class_<std::vector<int> >("vector_int")
    .def(vector_indexing_suite< std::vector<int> >())
  ;

  class_<Pythia, boost::noncopyable>("Pythia")

    // Setting up the engine
    .def("readString"       , &Pythia::readString, pythia_readString())
    .def("readFile"         , (bool (Pythia::*)(string, bool, int)) &Pythia::readFile, pythia_readFile())
    // TODO https://stackoverflow.com/questions/8140155/boost-python-confused-about-similar-constructor

    // setPDFPtr
    // setLHAupPtr
    // setDecayPtr
    // setRndmEnginePtr
    .def("setUserHooksPtr"  , &pythia_setUserHooks )
    // setMergingHooksPtr
    // setBeamShapePtr
    // setSigmaPtr
    // setResonancePtr
    // setShowerPtr

    // Main actions
    .def("init"               , (bool (Pythia::*)()) &Pythia::init) // Support only the no-arg signature, for 8.2
    .def("next"               , &Pythia::next)
    .def("forceTimeShower"    , &Pythia::forceTimeShower)
    .def("forceHadronLevel"   , &Pythia::forceHadronLevel)
    .def("moreDecays"         , &Pythia::moreDecays)
    .def("forceRHadronDecays" , &Pythia::forceRHadronDecays)
    .def("LHAeventList"       , &Pythia::LHAeventList)
    .def("LHAeventSkip"       , &Pythia::LHAeventSkip)

    // Consultings
    .def("stat"   , &Pythia::stat)
    .def("flag"   , &Pythia::flag)
    .def("mode"   , &Pythia::mode)
    .def("parm"   , &Pythia::parm)
    .def("word"   , &Pythia::word)
    
    // Public attribute members
    .def_readwrite("process"      , &Pythia::process)
    .def_readwrite("event"        , &Pythia::event)
    .def_readwrite("info"         , &Pythia::info)
    // settings
    .def_readwrite("particleData" , &Pythia::particleData)
    // rndm 
    // couplings
    // couplingsPtr 
    // slhaInterface
    // partonSystems
    // merging
    // mergingHooksPtr

  ;

}

