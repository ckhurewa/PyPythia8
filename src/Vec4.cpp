#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;


// From Basics.h
BOOST_PYTHON_MODULE(Vec4){

  class_<Vec4>("Vec4")
    .def(init<optional<double, double, double, double> >())

    // Arimethic operators
    // http://www.boost.org/doc/libs/1_35_0/libs/python/doc/v2/operators.html
    .def(-self)                 // __neg__
    .def(self   +   self)       // __add__
    .def(self   +=  self)       // __iadd__
    .def(self   -   self) 
    .def(self   -=  self) 
    .def(self   *   self)       // scalar prod
    .def(self   *   double())
    .def(self   /   double())

    .def(self_ns::str(self_ns::self))  // For operator <<
    // .def("__getitem__", &getitem, return_internal_reference<>() )  // new

    // Properties 
    .add_property( "px"     , (double(Vec4::*)()const)&Vec4::px, (void(Vec4::*)(double))&Vec4::px )
    .add_property( "py"     , (double(Vec4::*)()const)&Vec4::py, (void(Vec4::*)(double))&Vec4::py )
    .add_property( "pz"     , (double(Vec4::*)()const)&Vec4::pz, (void(Vec4::*)(double))&Vec4::pz )
    .add_property( "e"      , (double(Vec4::*)()const)&Vec4::e , (void(Vec4::*)(double))&Vec4::e  )

    // Read-only descriptor
    .add_property( "mCalc"  , &Vec4::mCalc )
    .add_property( "m2Calc" , &Vec4::m2Calc )
    .add_property( "pT"     , &Vec4::pT )
    .add_property( "pT2"    , &Vec4::pT2 )
    .add_property( "pAbs"   , &Vec4::pAbs )
    .add_property( "pAbs2"  , &Vec4::pAbs2 )
    .add_property( "eT"     , &Vec4::eT )
    .add_property( "eT2"    , &Vec4::eT2 )
    .add_property( "theta"  , &Vec4::theta )
    .add_property( "phi"    , &Vec4::phi )
    .add_property( "thetaXZ", &Vec4::thetaXZ )
    .add_property( "pPos"   , &Vec4::pPos )
    .add_property( "pNeg"   , &Vec4::pNeg )
    .add_property( "rap"    , &Vec4::rap )
    .add_property( "eta"    , &Vec4::eta )

    // Methods
    .def( "reset", &Vec4::reset )

    // Mutable methods (setters)
    .def( "p", (void(Vec4::*)(double, double, double, double)) &Vec4::p )
    .def( "p", (void(Vec4::*)(Vec4))                           &Vec4::p )

    .def("rescale3" , &Vec4::rescale3 )
    .def("rescale4" , &Vec4::rescale4 )
    .def("flip3"    , &Vec4::flip3 )
    .def("flip4"    , &Vec4::flip4 )

  ;

}

 

