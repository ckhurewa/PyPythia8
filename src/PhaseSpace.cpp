#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

// http://www.boost.org/doc/libs/1_55_0/libs/python/doc/tutorial/doc/html/python/exposing.html#python.class_virtual_functions
// http://www.boost.org/doc/libs/1_55_0/libs/python/doc/v2/register_ptr_to_python.html

class PhaseSpaceWrap: public PhaseSpace, public wrapper<PhaseSpace>{

public: 

  bool setupSampling(){
    return this->get_override("setupSampling")();
  }

  bool trialKin(bool inEvent = true, bool repeatSame = false){
    return this->get_override("trialKin")( inEvent, repeatSame );
  }

  bool finalKin(){
    return this->get_override("finalKin")();
  }

  double sigmaSumSigned(){
    return this->get_override("sigmaSumSigned")();
  }

  bool isResolved(){
    return this->get_override("isResolved")();
  }

};

BOOST_PYTHON_MODULE(PhaseSpace){

  class_<PhaseSpaceWrap, boost::noncopyable>("PhaseSpace", no_init)
  ;

  // register_ptr_to_python< shared_ptr<PhaseSpace> >();
}

