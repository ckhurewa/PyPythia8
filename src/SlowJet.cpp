#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(slowJet_list, SlowJet::list, 0, 2);

// from Analysis.h
BOOST_PYTHON_MODULE(SlowJet){
  
  class_<SlowJet, boost::noncopyable>("SlowJet", init<int, double, optional<double, double, int, int, SlowJetHook*, bool, bool> >())

    // Properties
    .add_property("sizeOrig"  , &SlowJet::sizeOrig )
    .add_property("sizeJet"   , &SlowJet::sizeJet )
    .add_property("sizeAll"   , &SlowJet::sizeAll )

    // Methods
    .def("analyze"    , &SlowJet::analyze )
    .def("setup"      , &SlowJet::setup )
    .def("doStep"     , &SlowJet::doStep )
    .def("doNSteps"   , &SlowJet::doNSteps )
    .def("stopAtN"    , &SlowJet::stopAtN )

    .def("pT"               , &SlowJet::pT )
    .def("y"                , &SlowJet::y)
    .def("phi"              , &SlowJet::phi )
    .def("p"                , &SlowJet::p )
    .def("m"                , &SlowJet::m )
    .def("multiplicity"     , &SlowJet::multiplicity)
    .def("list"             , &SlowJet::list, slowJet_list())
    .def("constituents"     , &SlowJet::constituents)
    .def("clusConstituents" , &SlowJet::clusConstituents)
    .def("jetAssignment"    , &SlowJet::jetAssignment)
    .def("removeJet"        , &SlowJet::removeJet)
  ;

}