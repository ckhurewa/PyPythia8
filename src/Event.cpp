#include "Pythia8/Pythia.h"
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

using namespace Pythia8;
using namespace boost::python;

//=============================================================================

const Particle& event_getitem(const Event& event, int index){
    if (index >= 0 && index < event.size()) {
        return event[index];
    } 
    PyErr_SetString(PyExc_IndexError, "index out of range");
    throw_error_already_set();
    return event[0]; // DUMMY to suppress warning. Shoud not be reachable though.
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(event_init, Event::init, 0, 3);

//=============================================================================

BOOST_PYTHON_MODULE(Event){

  class_<Event>("Event")
    .def(init<const Event&>()) // Allow copy constructor

    // Iterator protocol
    .def("__getitem__"  , &event_getitem, return_internal_reference<>() )
    .def("__len__"      , &Event::size )

    // Method
    .def("init"   , &Event::init, event_init())
    .def("clear"  , &Event::clear)
    .def("reset"  , &Event::reset)
    .def("list"   , (void (Event::*)() const) &Event::list)

    // Properties
    .add_property("scale"       , (double(Event::*)()const) &Event::scale      , (void(Event::*)(double)) &Event::scale       )
    .add_property("scaleSecond" , (double(Event::*)()const) &Event::scaleSecond, (void(Event::*)(double)) &Event::scaleSecond )
    
  ;

}