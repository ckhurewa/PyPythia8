#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

BOOST_PYTHON_MODULE(SigmaProcess){
  class_<SigmaProcess>("SigmaProcess", no_init)
  ;
}
