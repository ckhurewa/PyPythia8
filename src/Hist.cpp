#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(hist_fill, Hist::fill, 1, 2);

BOOST_PYTHON_MODULE(Hist){

  class_<Hist>("Hist")
    .def(init<std::string, int, double, double>())
    .def("fill", &Hist::fill, hist_fill())
    .def(self_ns::str(self_ns::self))  // For operator <<
  ;

}
