#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

BOOST_PYTHON_MODULE(Info){

  class_<Info>("Info")

    .def("list" , (void(Info::*)()) &Info::list )

    .add_property( "idA"  , &Info::idA)
    .add_property( "idB"  , &Info::idB)
    .add_property( "pzA"  , &Info::pzA)
    .add_property( "pzB"  , &Info::pzB)
    .add_property( "eA"   , &Info::eA)
    .add_property( "eB"   , &Info::eB)
    .add_property( "mA"   , &Info::mA)
    .add_property( "mB"   , &Info::mB)
    .add_property( "eCM"  , &Info::eCM)
    .add_property( "s"    , &Info::s)

    .add_property( "tooLowPTmin"      , &Info::tooLowPTmin)
    .add_property( "name"             , &Info::name)
    .add_property( "code"             , &Info::code)
    .add_property( "nFinal"           , &Info::nFinal)

    .add_property( "isResolved"       , &Info::isResolved)
    .add_property( "isDiffractiveA"   , &Info::isDiffractiveA)
    .add_property( "isDiffractiveB"   , &Info::isDiffractiveB)
    .add_property( "isDiffractiveC"   , &Info::isDiffractiveC)
    .add_property( "isNonDiffractive" , &Info::isNonDiffractive)
    .add_property( "isMinBias"        , &Info::isMinBias)
    
    .add_property( "isLHA"            , &Info::isLHA)
    .add_property( "atEndOfFile"      , &Info::atEndOfFile)
  ;

}
