#include "Pythia8/Pythia.h"
#include <boost/python.hpp>

using namespace Pythia8;
using namespace boost::python;

/* 
 * Note: There's an extra hack here. 
 *   Because Pythia8 doesn't allow the direct instantiation of UserHooks anyway,
 *   (it forces user to create a private subclass), we don't need to handle 
 *   the case where original (non-pure virtual) UserHooks is called!
 * See: http://www.boost.org/doc/libs/1_55_0/libs/python/doc/tutorial/doc/html/python/exposing.html#python.class_virtual_functions
 * 
 * EXAMPLE
 *

from PyPythia8 import Pythia, Particle, UserHooks

class Veto_fiducial(UserHooks):
  def doVetoProcessLevel( self, process ):
    d = { p.id:p for p in  process }
    Z = d[23]
    ## Veto ( see LHCb's fiducial region for Z )
    if not ( 60. < Z.m < 120. ):
      return True
    ## No veto, go.
    return False

p = Pythia()
veto = Veto_fiducial()  # Ownership
p.setUserHooksPtr( veto )
p.init( 'pwgevents.lhe' )


NOTE: Exposing abstract class used as pointer in other's argument.
1. The non-abstract wrapper class MUST be written for. The manual said so.
2. The method that originally took pointer to this as arg, will be wrapped
   to take ref and use ref instead. For example:

  const bool setUserHooksPtr(Pythia& pythia, UserHooks* hook) -->

  const bool pythia_setUserHooks(Pythia& pythia, UserHooks& hook){
    return pythia.setUserHooksPtr(&hook);
  }

}


 */
class UserHooksWrap: public UserHooks, public wrapper<UserHooks> {

public:     

  //----------------------------------------------------------------------------
  // The basic components
  // Expose the protected method to python
  //----------------------------------------------------------------------------

  void omitResonanceDecays(const Event& process, bool finalOnly = false){
    return UserHooks::omitResonanceDecays(process, finalOnly);
  }

  void subEvent(const Event& event, bool isHardest = true){
    return UserHooks::subEvent(event, isHardest);
  }

  using UserHooks::workEvent;

  //----------------------------------------------------------------------------
  // (i) Interrupt between the main generation levels
  //----------------------------------------------------------------------------

  bool initAfterBeams(){
    if(override f = this->get_override("initAfterBeams"))
      return true;
    return UserHooks::initAfterBeams();
  }

  bool canVetoProcessLevel(){
    if(override f = this->get_override("doVetoProcessLevel"))
      return true;
    return UserHooks::canVetoProcessLevel();
  }

  bool doVetoProcessLevel(Event& event){
    if(override f = this->get_override("doVetoProcessLevel"))
      return f(event);
    return UserHooks::doVetoProcessLevel(event);
  }

  bool canVetoPartonLevel(){
    if(override f = this->get_override("doVetoPartonLevel"))
      return true;
    return UserHooks::canVetoPartonLevel();
  } 

  bool doVetoPartonLevel(const Event& event){
    if(override f = this->get_override("doVetoPartonLevel"))
      return f(event);
    return UserHooks::doVetoPartonLevel(event);
  }

  bool canVetoPartonLevelEarly(){
    if(override f = this->get_override("doVetoPartonLevelEarly"))
      return true;
    return UserHooks::canVetoPartonLevelEarly();
  } 

  bool doVetoPartonLevelEarly(const Event& event){
    if(override f = this->get_override("doVetoPartonLevelEarly"))
      return f(event);
    return UserHooks::doVetoPartonLevelEarly(event);
  }

  //----------------------------------------------------------------------------
  // (ii) Interrupt during the parton-level evolution, at a pT scale
  //----------------------------------------------------------------------------

  bool canVetoPT(){
    override f1 = this->get_override("scaleVetoPT");
    override f2 = this->get_override("doVetoPT");
    if( f1 || f2 )
      return true;
    return UserHooks::canVetoPT();
  }

  double scaleVetoPT(){
    if(override f = this->get_override("scaleVetoPT"))
      return f();
    return UserHooks::scaleVetoPT();
  }  

  bool doVetoPT(int iPos, const Event& event){
    if(override f = this->get_override("doVetoPT"))
      return f( iPos, event );
    return UserHooks::doVetoPT( iPos, event );
  }

  //----------------------------------------------------------------------------
  // (v) Modify cross-sections or phase space sampling
  //----------------------------------------------------------------------------

  bool canModifySigma(){
    if(override f = this->get_override("multiplySigmaBy"))
      return true;
    return UserHooks::canModifySigma();
  } 

  double multiplySigmaBy( const SigmaProcess& sigmaProcessPtr, const PhaseSpace& phaseSpacePtr, bool inEvent){
    if(override f = this->get_override("multiplySigmaBy"))
      return f( &sigmaProcessPtr, &phaseSpacePtr, inEvent );
    return UserHooks::multiplySigmaBy( &sigmaProcessPtr, &phaseSpacePtr, inEvent );
  }

  //----------------------------------------------------------------------------
  // (vi) Reject the decay sequence of resonances
  //----------------------------------------------------------------------------

  bool canVetoResonanceDecays(){
    if(override f = this->get_override("doVetoResonanceDecays"))
      return true;
    return UserHooks::canVetoResonanceDecays();
  } 

  bool doVetoResonanceDecays(Event& event){
    if(override f = this->get_override("doVetoResonanceDecays"))
      return f(event);
    return UserHooks::doVetoResonanceDecays(event);
  }

  //----------------------------------------------------------------------------

};

//==============================================================================

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(UserHooksWrap_omitResonanceDecays, UserHooksWrap::omitResonanceDecays, 1, 2);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(UserHooksWrap_subEvent           , UserHooksWrap::subEvent           , 1, 2);


BOOST_PYTHON_MODULE(UserHooks){
  class_<UserHooksWrap, boost::noncopyable>("UserHooks")

    // Basic: Expose the protected 
    .def("omitResonanceDecays"    , &UserHooksWrap::omitResonanceDecays, UserHooksWrap_omitResonanceDecays())
    .def("subEvent"               , &UserHooksWrap::subEvent           , UserHooksWrap_subEvent())
    .def_readonly("workEvent"     , &UserHooksWrap::workEvent)

    // (i)
    .def("initAfterBeams"         , &UserHooksWrap::initAfterBeams)
    .def("doVetoProcessLevel"     , &UserHooksWrap::doVetoProcessLevel)
    .def("doVetoPartonLevel"      , &UserHooksWrap::doVetoPartonLevel)
    .def("doVetoPartonLevelEarly" , &UserHooksWrap::doVetoPartonLevelEarly)
    // (ii)
    .def("scaleVetoPT"            , &UserHooksWrap::scaleVetoPT)
    .def("doVetoPT"               , &UserHooksWrap::doVetoPT)
    // (v)
    .def("multiplySigmaBy"        , &UserHooksWrap::multiplySigmaBy)
    // (vi)
    .def("doVetoResonanceDecays"  , &UserHooksWrap::doVetoResonanceDecays)

  ;
}

