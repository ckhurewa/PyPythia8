#!/usr/bin/env python

"""

main01 is a part of the PYTHIA event generator.
Copyright (C) 2014 Torbjorn Sjostrand.
PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
Please respect the MCnet Guidelines, see GUIDELINES for details.

This is a simple test program. It fits on one slide in a talk.
It studies the charged multiplicity distribution at the LHC.

"""

from PyPythia8 import *

# Generator. Process selection. LHC initialization. Histogram.
pythia = Pythia()
pythia.readString("Beams:eCM = 8000.")
pythia.readString("HardQCD:all = on")
pythia.readString("PhaseSpace:pTHatMin = 20.")
pythia.init()

mult = Hist("charged multiplicity", 100, -0.5, 799.5)

# Begin event loop. Generate event. Skip if error. List first one.
for i in xrange(100):
  if not pythia.next():
    continue

  # Find number of all final charged particles and fill histogram.
  nCharged = 0
  for p in pythia.event:
    if p.isFinal and p.isCharged:
      nCharged += 1
    mult.fill( nCharged )
  
# End of event loop. Statistics. Histogram. Done.
pythia.stat()
print mult
