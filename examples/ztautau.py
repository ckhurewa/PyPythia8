#!/usr/bin/env python

__author__ = 'chitsanu.khurewathanakul@epfl.ch'


from collections import OrderedDict
from PyPythia8 import Pythia, Particle, UserHooks, Event
#
from rootpy.tree import Tree
from rootpy.io import root_open

class Veto_fiducial(UserHooks):
  """
  This hook with only select Z within LHCb fiducial acceptance. 
  Its number becomes the denominator in acceptance efficiency term.
  """
  def doVetoProcessLevel( self, process ):
    Z     = process.find(id=23)
    tau1  = Z.children(id=15)[0]
    tau2  = Z.children(id=-15)[0]
    ## Veto ( see LHCb's fiducial region for Z )
    if not ( 60. < Z.m < 120. ):
      return True
    if tau1.pT < 20.:
      return True
    if tau2.pT < 20.:
      return True
    if not ( 2.0 < tau1.eta < 4.5 ):
      return True
    if not ( 2.0 < tau2.eta < 4.5 ):
      return True
    ## No veto, go go go.
    return False

def prepare_brancing( p, dtype ):
  """
  Try to close other channel as much as possible without introducing bias.
  """
  if dtype=='mu_mu':
    p.readString('15:onMode = off')
    p.readString('15:onIfAny = 13')
    return
  if dtype=='h1_mu':
    p.readString('15:offIfAny = 11')  # Close taue
    p.readString('15:offIfAll = 211 211 211')  # Close h3++
    p.readString('15:offIfAll = 211 211 321')  # Close h3++
    p.readString('15:offIfAll = 211 321 321')  # Close h3++
    return
  raise ValueError('Unknown channel: %r'%dtype)

def validate_decay( p, dtype ):
  """
  Given ditau type, return True if given engine p having ditau decays as requested.
  """
  tt1,tt2 = dtype.split('_')
  Z       = p.event.find(id=23)
  t1,t2   = sorted(Z.children(), key=tauitype)
  return (tautype(t1)==tt1) and (tautype(t2)==tt2)

def tauitype( tau ):
  """
  return one of type (e, h1, h3, mu, other) of given Particle.
  """
  nQ = 0
  for c in tau.children():
    idabs = c.idAbs
    if idabs == 11:
      return 0
    if idabs == 13:
      return 3
    nQ += int(c.isCharged)
  if nQ==1:
    return 1 # tauh1
  if nQ==3:
    return 2 # tauh3
  return 4 # Other

def tautype( tau ):
  return ('e','h1','h3','mu','other')[tauitype(tau)]

def extract_ch( tau ):
  """
  Given tau-lepton, try to return the charge decay product.
  For simple tau, it can be simply electron, muon, pion/kaon.
  For tauh3, the Vec4 sum is returned. Thus, other operation using result of this
  function should be based on methods available in Vec4, not Particle class.
  """
  l = tau.children(isCharged=True)
  if len(l)==1: # Simple case
    return l[0]
  if len(l)==3: # tauh3
    raise NotImplementedError  
  raise NotImplementedError

def main(dtype):
  ## Prepare Pythia
  p = Pythia()
  p.readString('Next:numberShowEvent = 0')
  p.readString('HadronLevel:all = off') # For custom-repeating decay
  p.readString("Beams:frameType = 4")
  p.readString("Beams:LHEF = pwgevents.lhe")
  prepare_brancing( p, dtype )

  ## Process-level veto (LHCb Fiducial)
  veto = Veto_fiducial()  # Ownership
  p.setUserHooksPtr( veto )

  p.init()

  ## Prepare output
  fout = root_open('tuple.root', 'recreate')
  tree = Tree(dtype)
  prefixes = [ s1+s2 for s1 in ('Z','tau1','tau2','tau1ch','tau2ch') for s2 in ('top','bot') ]
  attrs    = ( 'e', 'pT', 'pz', 'eta', 'phi' )
  branches = { '%s_%s'%(prefix,attr.upper()):'F' for prefix in prefixes for attr in attrs }
  branches['Ztop_M'] = 'F'
  branches['Zbot_M'] = 'F'
  branches['ditau_chtop_M'] = 'F'
  tree.create_branches(OrderedDict(sorted(branches.items())))

  i = 0
  while p.next():
    i+=1
    if i%1000==1:
      print 'Processing event: ', i

    ## Save event before hadronization
    event0 = Event(p.event) # Make a copy 
    valid  = False

    ## Keep hadronizing
    while not valid:
      p.event = event0
      success = p.forceHadronLevel(False)
      if success and validate_decay( p, dtype ):
        valid = True

    ## Skip bad hadronize immediately, other keep continuing
    # while not valid:
    #   p.event = event0
    #   success = p.forceHadronLevel(False)
    #   if not success:
    #     break # break hadron loop to skip this event entirely
    #   if validate_decay( p, dtype ):
    #     valid = True
    # if not valid: # probably from failed hadronisation
    #   continue


    ## Get hard event particles
    Ztop    = p.event.find(id=23).topCopyId()
    Zbot    = Ztop.botCopyId()
    tau1top, tau2top = sorted(Ztop.children(), key=tauitype)
    tau1bot = tau1top.botCopyId()
    tau2bot = tau2top.botCopyId()

    ## Get tau decay products, depends on dtype
    tau1chtop = extract_ch( tau1bot )
    tau2chtop = extract_ch( tau2bot )
    tau1chbot = tau1chtop.botCopyId()
    tau2chbot = tau2chtop.botCopyId()

    ## Fill the output!
    for pname in prefixes:
      part = locals()[pname]
      for attr in attrs:
        key = pname+'_'+attr.upper()
        val = getattr( part, attr )
        setattr( tree, key, val )
    ## Manual
    tree.Ztop_M = Ztop.m
    tree.Zbot_M = Zbot.m
    tree.ditau_chtop_M = (tau1chtop.p+tau2chtop.p).mCalc
    tree.fill()

  ## Closing
  tree.write()
  fout.close()
  p.stat()

if __name__ == '__main__':
  main('h1_mu')
