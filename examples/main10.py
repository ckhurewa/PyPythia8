#!/usr/bin/env python

"""

main10 is a part of the PYTHIA event generator.
Copyright (C) 2014 Torbjorn Sjostrand.
PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
Please respect the MCnet Guidelines, see GUIDELINES for details.

Example how you can use UserHooks to trace pT spectrum through program,
and veto undesirable jet multiplicities.

"""

from PyPythia8 import *

#===============================================================================

# Put histograms here to make them global, so they can be used both
# in MyUserHooks and in the main program.

pTtrial   = Hist("trial pT spectrum", 100, 0., 400.)
pTselect  = Hist("selected pT spectrum (before veto)", 100, 0., 400.)
pTaccept  = Hist("accepted pT spectrum (after veto)", 100, 0., 400.)
nPartonsB = Hist("number of partons before veto", 20, -0.5, 19.5)
nJets     = Hist("number of jets before veto", 20, -0.5, 19.5)
nPartonsA = Hist("number of partons after veto", 20, -0.5, 19.5)
nFSRatISR = Hist("number of FSR emissions at first ISR emission", 20, -0.5, 19.5)

#===============================================================================

class MyUserHooks(UserHooks):
  """
  Write own derived UserHooks class.
  """

  def __init__(self):
    """
    Constructor creates anti-kT jet finder with (-1, R, pTmin, etaMax).
    """
    super(MyUserHooks,self).__init__()
    # The anti-kT (or kT, C/A) jet finder.
    self.slowJet = SlowJet(-1, 0.7, 10., 5.)
    # Save the pThat scale.
    self.pTHat   = 0.

  def multiplySigmaBy(self, sigmaProcessPtr, phaseSpacePtr, inEvent):
    """
    Allow process cross section to be modified, which gives access to the event 
    at the trial level, before selection.
    """
    # All events should be 2 -> 2, but kill them if not.
    if (sigmaProcessPtr.nFinal != 2): 
      return 0.
    
    # Extract the pT for 2 -> 2 processes in the event generation chain
    # (inEvent = false for initialization).
    if (inEvent):
      self.pTHat = phaseSpacePtr.pTHat
      # Fill histogram of pT spectrum.
      pTtrial.fill( self.pTHat )
    
    # Here we do not modify 2 -> 2 cross sections.
    return 1.
  
  def scaleVetoPT(self):
    """
    Do the veto test at a pT scale of 5 GeV.
    """
    return 5.

  def doVetoPT(self, iPos, event):
    """
    Access the event in the interleaved evolution.
    """
    # iPos <= 3 for interleaved evolution; skip others.
    if (iPos > 3):
      return False
    
    # Fill histogram of pT spectrum at this stage.
    pTselect.fill(self.pTHat)

    # Extract a copy of the partons in the hardest system.
    self.subEvent(event);
    nPartonsB.fill( len(self.workEvent) )

    # Find number of jets with given conditions.
    self.slowJet.analyze(event)
    nJet = self.slowJet.sizeJet
    nJets.fill( nJet )

    # Veto events which do not have exactly three jets.
    if (nJet != 3): 
      return True

    # Statistics of survivors.
    nPartonsA.fill( len(self.workEvent) )
    pTaccept.fill( self.pTHat )

    # Do not veto events that got this far.
    return False

  def doVetoStep(self, iPos, nISR, nFSR, event):
    """
    Access the event in the interleaved evolution after first step.
    """
    # Only want to study what happens at first ISR emission
    if (iPos == 2 and nISR == 1): 
      nFSRatISR.fill( nFSR )
    # Not intending to veto any events here.
    return False

#===============================================================================

def main():

  # Generator.
  pythia = Pythia()

  #  Process selection. No need to study hadron level.
  pythia.readString("HardQCD:all = on")
  pythia.readString("PhaseSpace:pTHatMin = 50.")
  pythia.readString("HadronLevel:all = off")

  # Set up to do a user veto and send it in.
  myUserHooks = MyUserHooks()
  pythia.setUserHooksPtr( myUserHooks )
 
  # Tevatron initialization.
  pythia.readString("Beams:idB = -2212")
  pythia.readString("Beams:eCM = 1960.")
  pythia.init()
   
  # Begin event loop.
  for iEvent in xrange(1000):
    pythia.next()

  # Statistics. Histograms.
  pythia.stat()
  print pTtrial, pTselect, pTaccept, nPartonsB, nJets, nPartonsA, nFSRatISR

#===============================================================================

if __name__ == '__main__':
  main()
