#!/usr/bin/env python
# Pythia event parser

"""
Parse the event table from Pythia8 Event Generator into more human-readible format.
This makes debugging MC event much faster!

*Depth-code* is a string represent several information for the printer to determine
what kind of formatting/indent shall be used.

* Length of DC represent the depth of each particle. The root one has len==0, etc.
* Last digit is... 
   - 2 if this particle is the only child of his parent,
   - 1 if this particle is last child of his parent (whom having >1 children). 
   - 0 otherwise
* Whole DC excluding the last digit, is the DC of this particle's parent.

"""

import sys

COL_INDEX   = 0
COL_ID      = 1
COL_NAME    = 2
COL_STATUS  = 3
COL_CHILD1  = 6
COL_CHILD2  = 7
COL_ENERGY  = 13
COL_MASS    = 14

#==============================================================================
# PARSER
#==============================================================================

def isLineBegin(line):
   """Return True if the given string is the start of event-block"""
   return "PYTHIA Event Listing  (complete event)" in line

def isLineEnd(line):
   """Return True if the given string is the end of event-block"""
   return "End PYTHIA Event Listing" in line 

def ignoreThisLine(line):
   """Return True if this line should be skipped. Usually it's blank"""
   if "status" in line: return True
   if "Charge sum" in line: return True
   return len(line.strip())==0

def parseData(source):
   """
   Starting from raw file, extract the block of Pythia8 event found.

   :param source: A string to the location of source file
   :return: A python-list of event (Each event is a python-list of Pythia8 row)

   """

   print "\nParsing: " + source
   list_event = []
   with open(source) as f:
      hasBegun = False
      for line in f:
         if isLineBegin(line):
            hasBegun = True
            event = list()
            continue
         if isLineEnd(line):
            hasBegun = False
            list_event.append(event)
            continue
         if ignoreThisLine(line):
            continue
         if hasBegun:
            event.append(line.split())
   print "Found %i event(s) in the given file" % len(list_event)
   return list_event


#==============================================================================
# HELPER FUNCTIONS on particle properties
#==============================================================================

def isStable(p):
   """Return True if the particle is stable. In Pythia8, status code is positive"""
   return int(p[COL_STATUS])>0

def iBotCopy(event, index):
   """
   Get the index of same particle at last-point-in-time. See Pythia8 manual.

   :param event: Event table to be used as reference.
   :param index: Index of particle to start from.
   :return: integer index of row in event table.

   """

   ic1 = int(event[index][COL_CHILD1])
   ic2 = int(event[index][COL_CHILD2])
   if ic1==0 and ic2==0: return index
   if ic1==ic2: return iBotCopy(event, ic1)
   return index

def getChildrenList(p):
   """
   Get the list of indices represents children of this particle. 
   See Pythia8 manual for indices convention.

   :param p: A particle, i.e.,one row from event table (a list)
   :return: List of integers represents indices of children. Empty if no children.

   """

   ic1 = int(p[COL_CHILD1])
   ic2 = int(p[COL_CHILD2])
   if ic1>0 and ic2==0:
      return [ic1]
   elif ic1>0 and ic2>0 and ic1<ic2:   # Ranged children
      return [i for i in range(ic1, ic2+1)]
   elif ic1>0 and ic2>0 and ic1>ic2:   # no-ranged children
      return [ic1,ic2]
   else: 
      return []

def hasChildren(p):
   """Return True if this particle has any children"""
   return len(getChildrenList(p))>0


#==============================================================================
# PRINTERS
#==============================================================================

def getDepthCode(parentCode, i, total):
   """
   Calculate the depth-code of this particle.

   :param parentCode: Depth-code of this particle's parent.
   :param i: Index of child, relative to this particle's parent.
   :param total: Total number of siblings this particle has.
   :return: *Depth-code* of this particle.

   """
   return parentCode + ("2" if total==1 else "1" if i+1==total else "0")

def printIndent(depth_code, row=None):
   if len(depth_code)>0:
      for c in depth_code[:-1]:      
         print (" |  " if c=='0' else "    "),
      print ("\n" if row is None else " |--"),

def printParticleLeftPane(depth_code, row, max_depth):
   depth = len(depth_code)
   print '{0:<10}'.format(row[COL_NAME]) + "     "*(max_depth-depth),


def printParticleRightPane(row, ibot):
   row_format = "| I={:<4} -> {:<4} | ST={:<4} | M={:<4}"
   print row_format.format(row[COL_INDEX], ibot, row[COL_STATUS], row[COL_MASS])


def printEndSet(depth_code, row, max_depth):
   depth = len(depth_code)
   # if all([c=='1' for c in depth_code]): print depth_code
   if depth>0 and depth_code[-1]=='1' and not (hasChildren(row) and depth<max_depth) and not all([c=='1' for c in depth_code]):
      printIndent(depth_code)

def printLine(row, ibot, depth_code, max_depth):
   printIndent             (depth_code, row)
   printParticleLeftPane   (depth_code, row, max_depth)
   printParticleRightPane  (row, ibot)
   printEndSet             (depth_code, row, max_depth)

def printPythiaLine(event, index, depth_code="", max_depth=3):
   particle     = event[index]
   indexBotCopy = iBotCopy(event, index) 
   printLine(particle, indexBotCopy, depth_code, max_depth)
   if not isStable(particle) and len(depth_code) < max_depth:
      l = getChildrenList(event[indexBotCopy])
      for i,key_child in enumerate(l):
         printPythiaLine(event, key_child, getDepthCode(depth_code, i, len(l)), max_depth)

#----------------------------------------------------------

def processEvent(event, max_depth=3):
   """
   For each event received, print header & begin parsing down the tree

   :param event: Event table (list of list)
   :param max_depth: Maximum depth that the event expands.
   
   """

   parent1 = event[3]
   parent2 = event[4]
   l1 = getChildrenList(parent1)
   l2 = getChildrenList(parent2)
   assert l1==l2
   print parent1[COL_NAME] + " + " + parent2[COL_NAME] + " ==> " + " + ".join([event[key][COL_NAME] for key in l1])
   for index_child in l1:
      print ""
      printPythiaLine(event, index_child, max_depth=max_depth)

def processEventModeB(event, max_depth=3):
   for index_child in (1,2):
      print ""
      printPythiaLine(event, index_child, max_depth=max_depth)


def printHeader(index):
   print 
   print "-" * 80
   print ("Event #%i: " % index),

def printFooter():
   print "-" * 80

#----------------------------------------------------------

def main():
   if len(sys.argv)<2: assert False, "No input. Abort."
   max_depth   = 3 if len(sys.argv)<3 else int(sys.argv[2])
   list_event  = parseData(sys.argv[1])
   for i,event in enumerate(list_event):
      printHeader(i)
      # processEvent(event, max_depth)
      processEventModeB(event, max_depth)
      printFooter()
      

if __name__ == '__main__':
   main()
