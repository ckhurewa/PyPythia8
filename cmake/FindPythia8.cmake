#-----------------------------#
# CMake find Pythia 8 library #
#-----------------------------#
#
# This module defines
#
# PYTHIA8_FOUND        whether Pythia8 was found
# PYTHIA8_INCLUDE_DIRS where to locate Pythia.h file
# PYTHIA8_LIBRARIES    the libraries (+lhapdf, lhapdfdummy, hepmc)
#
# Required 
# - PYTHIA8DATA
#

## Pythia8 not found as default
set(PYTHIA8_FOUND false)

## Required envvar
if(NOT DEFINED ENV{PYTHIA8DATA})
  message(FATAL_ERROR "$PYTHIA8DATA environment variable undefined.")
endif()

## use PYTHIA8DATA as base for PYTHIA8_ROOT
## From 8.2  : ./share/Pythia8/xmldoc
## From 8.175: ./xmldoc
set(PYTHIA8_ROOT $ENV{PYTHIA8DATA})
string(REPLACE /share/Pythia8/xmldoc / PYTHIA8_ROOT ${PYTHIA8_ROOT})
string(REPLACE /xmldoc / PYTHIA8_ROOT ${PYTHIA8_ROOT})

## Find the Pythia8 include directory
# 8.175: Pythia.h (IGNORE COMPAT)
# after: Pythia8/Pythia.h
find_path(PYTHIA8_INCLUDE_DIRS Pythia8/Pythia.h
	PATHS ${PYTHIA8_ROOT}/include
)
 
message(STATUS "Found Pythia8 source: ${PYTHIA8_INCLUDE_DIRS}")
if(NOT PYTHIA8_INCLUDE_DIRS)
  message(FATAL_ERROR "$PYTHIA8_INCLUDE_DIRS undefined.")
endif()


## Find the Pythia8 library
find_library(PYTHIA8_LIBRARY pythia8
	PATHS ${PYTHIA8_ROOT}/lib
)
message(STATUS "Found Pythia8 library: ${PYTHIA8_LIBRARY}")
set(PYTHIA8_LIBRARIES ${PYTHIA8_LIBRARY})


# ## (OPTIONAL) pythia8lhapdf6 (8.2+)
# find_library(PYTHIA8_LIBRARY_PYTHIA8LHAPDF6 pythia8lhapdf6
# 	PATHS ${PYTHIA8_ROOT}/lib
# )
# if(PYTHIA8_LIBRARY_PYTHIA8LHAPDF6)
# 	message(STATUS "Found pythia8lhapdf6: ${PYTHIA8_LIBRARY_PYTHIA8LHAPDF6}")
# 	list(APPEND PYTHIA8_LIBRARIES ${PYTHIA8_LIBRARY_PYTHIA8LHAPDF6})
# endif()


## (OPTIONAL) lhapdfdummy (pre 8.2)
find_library(PYTHIA8_LIBRARY_LHAPDFDUMMY lhapdfdummy
	PATHS ${PYTHIA8_ROOT}/lib
)
if(PYTHIA8_LIBRARY_LHAPDFDUMMY)
	message(STATUS "Found lhapdfdummy: ${PYTHIA8_LIBRARY_LHAPDFDUMMY}")
	list(APPEND PYTHIA8_LIBRARIES ${PYTHIA8_LIBRARY_LHAPDFDUMMY})
endif()

# ## (OPTIONAL) pythia8tohepmc
# find_library(PYTHIA8_LIBRARY_PYTHIA8TOHEPMC pythia8tohepmc
# 	PATHS ${PYTHIA8_ROOT}/lib
# )
# if(PYTHIA8_LIBRARY_PYTHIA8TOHEPMC)
# 	message(STATUS "Found pythia8tohepmc: ${PYTHIA8_LIBRARY_PYTHIA8TOHEPMC}")
# 	list(APPEND PYTHIA8_LIBRARIES ${PYTHIA8_LIBRARY_PYTHIA8TOHEPMC})
# endif()


# ## (OPTIONAL) LHAPDF6 library
# # Run the pkg-config executable to get information
# set (EXECUTE_COMMAND $ENV{SHELL} ${PYTHIA8_ROOT}/bin/pythia8-config --lhapdf6)
# execute_process(
# 	COMMAND ${EXECUTE_COMMAND}
# 	OUTPUT_VARIABLE RAW_LHAPDF6
# )
# if (${RAW_LHAPDF6} MATCHES "-L.*")
# 	# Extract the string
# 	string(REGEX MATCH "-L.* " RAW_LHAPDF6 ${RAW_LHAPDF6})
# 	string(REPLACE "/lib " "" RAW_LHAPDF6 ${RAW_LHAPDF6})
# 	string(REPLACE "-L" "" RAW_LHAPDF6 ${RAW_LHAPDF6})
# 	string(STRIP RAW_LHAPDF6 ${RAW_LHAPDF6})
# 	# Find 
# 	find_library(PYTHIA8_LIBRARY_LHAPDF6 LHAPDF
# 		PATHS ${RAW_LHAPDF6}/lib
# 	)
# 	# Collect info
# 	if(PYTHIA8_LIBRARY_LHAPDF6)
# 		message(STATUS "Found LHAPDF6: ${PYTHIA8_LIBRARY_LHAPDF6}")
# 		list(APPEND PYTHIA8_LIBRARIES ${PYTHIA8_LIBRARY_LHAPDF6})
# 	endif()
# endif()


## Mark as advanced settings 
MARK_AS_ADVANCED(PYTHIA8_FOUND PYTHIA8_INCLUDE_DIRS PYTHIA8_LIBRARIES)

