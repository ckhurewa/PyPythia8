#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import call

def test_run(tmpdir):
  """
  Run only first one, the other are slow.
  """
  with tmpdir.as_cwd() as olddir:
    src = str(olddir.join('examples/main01.py'))
    code = call(['python', src])
    assert code == 0
