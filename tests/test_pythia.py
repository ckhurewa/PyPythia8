#!/usr/bin/env py.test

import pytest
from PyPythia8 import Pythia

def test_init():
  Pythia()

@pytest.fixture
def p():
  return Pythia()

def test_init2(p):
  p.init()

def test_next(p):
  p.init()
  p.next()

def test_readString_good(p):
  assert p.readString('Next:numberShowEvent = 0') 
  
def test_readString_bad(p):
  assert not p.readString('BLAH_BLAH=BLAH') 

def test_setUserHookPtr(p):
  from PyPythia8 import UserHooks
  class Hook(UserHooks):
    pass
  hook = Hook()
  assert p.setUserHooksPtr(hook)
