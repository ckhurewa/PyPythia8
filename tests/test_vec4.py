#!/usr/bin/env py.test

import pytest
from PyPythia8 import Vec4

def test_init_default():
  Vec4()

def test_init_4args():
  Vec4(0.) 
  Vec4(0., 1.) 
  Vec4(0., 1., 2.) 
  Vec4(0., 1., 2., 3.) 
  Vec4(0, 1, 2, 3) # int, instead of float

@pytest.fixture
def v():
  return Vec4(0, 1, 2, 3)

## descriptor protocol. Test this first.
def test_get_px(v):
  assert v.px == 0
  assert v.py == 1
  assert v.pz == 2
  assert v.e  == 3

def test_set_px(v):
  v.px = 5
  assert v.px == 5
  v.py = 6
  assert v.py == 6
  v.pz = 7
  assert v.pz == 7
  v.e = 8
  assert v.e == 8

def test_get_pT2(v):
  assert v.pT2 == (v.px**2 + v.py**2)

def test_set_p_4args(v):
  v.p( 2, 3, 4, 5 )
  assert v.px == 2
  assert v.py == 3
  assert v.pz == 4
  assert v.e  == 5

def test_set_p_1arg(v):
  v.p(Vec4(3,4,5,6))
  assert v.px == 3
  assert v.py == 4
  assert v.pz == 5
  assert v.e  == 6

def test_reset(v):
  v.reset()
  assert v.px == v.py == v.pz == 0

def test_operator_neg(v):
  v2 = -v 
  assert v2.px == 0
  assert v2.py == -1
  assert v2.pz == -2
  assert v2.e  == -3

def test_operator_add(v):
  v2 = v+v
  assert v2.px == 0
  assert v2.py == 2
  assert v2.pz == 4
  assert v2.e  == 6

def test_operator_iadd(v):
  v += Vec4(3,3,3,3)
  assert v.px == 3
  assert v.py == 4
  assert v.pz == 5
  assert v.e  == 6

def test_operator_imul(v):
  v *= 3
  assert v.px == 0
  assert v.py == 3
  assert v.pz == 6
  assert v.e  == 9

def test_operator_mul_vv(v):
  # remark the metric
  assert (Vec4(1,2,3,4) * Vec4(2,3,4,5)) == (1*2 + 2*3 + 3*4 - 4*5)
