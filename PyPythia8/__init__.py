"""

Python-interfaced library for `Pythia8` Event Generator.

"""

import os
import sys
from glob import glob

## Import all into this package
path = os.path.abspath(os.path.join(__path__[0],'../lib'))
sys.path.append(path)
fpaths = glob(os.path.join(path, '*.so'))
assert fpaths, 'No *.so found at: %s'%path
for fpath in fpaths:
  fname = os.path.split(fpath)[-1].split('.')[0]
  mod   = __import__(fname, globals(), locals())
  globals().update(mod.__dict__)

#----------#
# PARTICLE #
#----------#

# Particle_HEADER = "index     id  name            mother   daughter        E       pt       pz      eta      phi"

def _particles_search( cont, **kwargs ):
  """
  Given a particle-iterable container, search by attributes.
  e.g., putting idAbs=5 --> check for p.idAbs()==5
  If no filter is given, return all particles in that container

  raise AttributeError if invalid key is given.
  """
  for p in cont:
    valid = True
    for key,val in kwargs.iteritems():
      valid &= (getattr(p,key)==val)
    if valid:
      yield p 

def Particle_children(self, **kwargs):
  """
  Return list of children of this Particle. Relies in evtPtr.
  Optionally, if kwarg is supplied, the children is filtered by respective method.
  """
  parts = (self.event[i] for i in self.botCopyId().daughterList)
  return list(_particles_search( parts, **kwargs ))

Particle.children   = Particle_children
Particle.botCopyId  = lambda p: p.event[p.iBotCopyId]
Particle.topCopyId  = lambda p: p.event[p.iTopCopyId]


#------#
# VEC4 #
#------#

## Aliasing
Vec4.m = Vec4.mCalc

#-------#
# EVENT #
#-------#

def Event_getitem(func):
  """
  Patch to "allow" pointer-to-event to be accessible from returning Particle.

  Accessible with attribute `event`
  >> p = Event[i]
  >> p.event 

  This is a workaround, in analogous to method setEvtPtr(*Event) to its 
  private attribute evtPtr.
  """
  # @wraps(func)
  def wrap( self, index ):
    p = func( self, index )
    p.event = self 
    return p
  return wrap

def Event_findall( self, **kwargs ):
  """
  Find all particles matching the spec.
  """
  return _particles_search( self, **kwargs )

def Event_find( self, **kwargs ):
  """
  Return first particle in event that match the search query.

  See _particles_search.
  """
  for p in _particles_search( self, **kwargs ):
    return p # only the first item is needed

Event.__getitem__ = Event_getitem(Event.__getitem__)
Event.findall     = Event_findall
Event.find        = Event_find
