PyPythia8
=========

[![pipeline status](https://gitlab.com/ckhurewa/PyPythia8/badges/master/pipeline.svg)](https://gitlab.com/ckhurewa/PyPythia8/commits/master)
[![coverage report](https://gitlab.com/ckhurewa/PyPythia8/badges/master/coverage.svg)](https://gitlab.com/ckhurewa/PyPythia8/commits/master)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


Introduction
------------

`PyPythia8` is a python-interfaced package for high energy physics Monte-Carlo 
event generator `Pythia8`, just like `PyROOT` to `ROOT`.

Here's a hello world example:

```python
from PyPythia8 import Pythia
pythia = Pythia()
pythia.readString("Beams:eCM = 8000.")
pythia.readString("HardQCD:all = on")
pythia.init()
for i in xrange(10):
  if not pythia.next():
    continue
pythia.stat()
```

`PyPythia8` uses `boost-python` library to provide the python interface to
existing `Pythia8` library, without having to reinstall it.
*There is no changes in physics*. They are completely taken care at the C++ level.
This has several benefits: 

- User can feel safe and confident with the analysis under study.

- It can easily migrate to the newest version of C++ Pythia8, 
  gaining new functionalities and improvements without hassle.

- Running interactively on python allows immediate inspection of the code,
  making your physics analysis easier to study.

- It's fast (the hard work is still carry out at C++ level). 
  Once you tried it, it will be difficult to get back :)


`Pythia8` is a splendid work by Torbjörn Sjöstrand et al.
See also <http://home.thep.lu.se/~torbjorn/Pythia.html>, arXiv:1410.3012


Contact: <chitsanu.khurewathanakul@gmail.com>

Author's note:: 

1. I only wrote this library during my PhD at EPFL+LHCb, so the interfacing is not
designed to be complete at this level, just the core classes used in my analysis.

2. In version 8.219 (10 May 2016), the official Pythia8 start to have python wrapper too.
However, using `SWIG` for auto-generation seems heavy and it requires the 
reinstallation of full Pythia8. This package is build on top of existing pythia8
installation, thus can easier be installed on CERN lxplus.

3. There is also similar project `numpythia` (https://github.com/scikit-hep/numpythia),
which provide a binding to `numpy` directly, with a caveat that there is internal
copy of pythia8 bundled in the installation as well.


Installation
------------

`PyPythia8` installation is written with `CMake`. A priori, there are 3 dependencies: 

1. `python` (2.6+)
    This should automatically be taken care by CMake.

2. `Pythia8` (8.185+)
    User should set `PYTHIA8DATA` environmental variable, which points to the 
    `xmldoc` directory as instructed by the vanilla `Pythia8` documentation.
    The location where this variable points to is used to refer the sources
    and libraries to be searched by `CMake`.

3. `Boost` (1.55+)
    This is used to interface `Pythia8` to `python`. CMake should be able to 
    find appropriate installation automatically as well. 
      User may provide env variable `BOOST_ROOT` pointing to their prefered 
    installation path, which should contains `include` and `lib` directories.


Once the dependencies requirement are met, it can be compiled in cmake-style.
From the root directory (containing `CMakeList.txt`), one can do:

```bash
mkdir build; cd build
cmake ..
make
```

and when the compilation completes, bind the package to your shell:

```bash
export PYTHONPATH="$PYTHONPATH:/path/to/PyPythia8/python"
```


### Installation recipe: CERN's `lxplus`

All dependencies can be found in LCG releases, for instance:

```sh
## Pick the LCG version to use
export MYLCG="/afs/cern.ch/sw/lcg/releases/LCG_79"

## Pick version of Pythia8, for example, 8.185
export PYTHIA8DATA="$MYLCG/MCGenerators/pythia8/185/$CMTCONFIG/share/Pythia8/xmldoc"

## Pick Boost installation
export BOOST_ROOT=$MYLCG/Boost/1.55.0_python2.7/$CMTCONFIG

## Default libgcc at /usr/lib/libstdc++.so is not compat with LCG Pythia8.
# so the workaround is needed.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MYLCG/gcc/4.9.3/x86_64-slc6/lib64

## In some rare case, the `cc` compiler needs to match with the precompiled 
# boost library as well.
# export CC $MYLCG/gcc/4.9.3/x86_64-slc6/bin/gcc
```


### Installation recipe: CERN's `lxplus` (LHCb user)

For LHCb user, running `SetupGauss` before installing will take care of all dependencies
(this also means you have to call `SetupGauss` to prepare environment when you use this
library at runtime too).

Tested with: `x86_64-slc6-gcc49-opt`, `Gauss v49r1`, `LCG_79`, `Pythia 8.186`, `Boost 1.55`


List of changes
---------------

(On-going list)

Most syntaxes are left unchaged, such that the original documentation can be used
directly without incompatibility. Notable changes are:

- Makes idiom to be pythonic

- getter&setter --> descriptor protocol

- Eliminated the need for `UserHooks::canXXX` virtual method. If the corresponding
  method (`doXXX`) has been defined in the subclass, then this is implicitly true.

- `x.size()` also has `len(x)`.
