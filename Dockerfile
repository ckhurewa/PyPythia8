## Dockerfile to build base image with cmake, python, boost-python,
## ready for PyPythia8 build+test CI-CD.

## Using ubuntu base image
FROM ubuntu
RUN apt-get update -q
RUN apt-get install -yqq build-essential cmake python-dev libboost-all-dev wget

## Install Pythia8 from source
ENV PYTHIA8_VERSION pythia8185
RUN wget http://home.thep.lu.se/~torbjorn/pythia8/$PYTHIA8_VERSION.tgz && \
    tar xvfz $PYTHIA8_VERSION.tgz && \
    cd $PYTHIA8_VERSION && \
    ./configure --enable-shared --prefix=/usr/local && \
    make && \
    make install && \
    rm -rf /$PYTHIA8_VERSION*
ENV PYTHIA8DATA /usr/local/xmldoc
WORKDIR /

## Install pytest via pip
RUN apt-get install -yqq python-pip python-dev && \
    pip install pytest pytest-cov pytest-flakes

# ## temp: build for python testing
# COPY cmake/ ./cmake
# COPY src/ ./src
# COPY CMakeLists.txt .
# RUN mkdir build
# WORKDIR build
# RUN cmake .. && make
# RUN cp /root/lib/*.so /usr/local/lib
# WORKDIR /root
# RUN apt-get install -yqq python-pip python-dev build-essential
# RUN pip install --upgrade pip
